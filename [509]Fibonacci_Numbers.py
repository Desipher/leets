# complete

class Solution:
    def fib_efficient_everything(self, n: int) -> int:
        if n == 0 or n == 1:
            return n
        prev = 0
        curr = 1
        for i in range(n - 1):
            next = prev + curr
            prev = curr
            curr = next
        return curr

    def fib_efficient_memory(self, n: int) -> int:
        if n == 0 or n == 1:
            return n
        tab = [0] * (n + 1)
        tab[1] = 1
        for i in range(2, n + 1):
            tab[i] = tab[i - 1] + tab[i - 2]
        return tab[-1]

    # This one is from leetcode submissions. It helps me check my own solution agaisnt my own test cases
    def correct_solution(self, n: int) -> int:
        if n == 0:
            return 0
        if n == 1:
            return 1
        n1 = 0
        n2 = 1
        for i in range(n - 1):

            n2, n1 = n2 + n1, n2

        return n2


inputs = [
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 999
]

for i in inputs:
    result = Solution().fib_efficient_everything(i)
    correct_result = Solution().correct_solution(i)
    if result == correct_result:
        print('✔️    ', result, '    👉  ', correct_result)
    else:
        print('❌   ', result, '    👉  ', correct_result)
# EXAMPLE TEST CASES
#
