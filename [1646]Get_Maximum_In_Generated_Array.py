# complete

# Algorithm used to generate the array in the question.
# nums[0] = 0
# nums[1] = 1
# nums[2 * i] = nums[i] when 2 <= 2 * i <= n        <-> Example; nums[(1 * 2) = 2] = nums[1] = 1, where i = 1
# nums[2 * i + 1] = nums[i] + nums[i + 1] when 2 <= 2 * i + 1 <= n      <-> Example; nums[(1 * 2) + 1 = 3] = nums[1] + nums[2] = 1 + 1 = 2, where i = 1


class Solution:
    def getMaximumGenerated(self, n: int) -> int:
        if n == 0 or n == 1:
            return n
        nums = [0] * (n + 1)
        nums[1] = 1

        for i in range(2, n + 1):
            if (i % 2 == 0):
                nums[i] = nums[i // 2]
            else:
                nums[i] = nums[(i - 1) // 2] + nums[((i - 1) // 2) + 1]

        return max(nums)

    def leetcode_solution(self, n):
        nums = [0] * (n + 2)
        nums[1] = 1
        for i in range(2, n + 1):
            nums[i] = nums[i // 2] + nums[(i // 2) + 1] * (i % 2)

        return max(nums[:n + 1])


inputs = [
    0,
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15,
    16,
    17,
    18,
    19,
    20,
    21,
    22,
    23,
    24,
    25,
    26,
    27,
    28,
    29,
    30,
    31,
    32,
    33,
    34,
    35,
    36,
    37,
    38,
    39,
    40,
    41,
    42,
    43,
    44,
    45,
    46,
    47,
    48,
    49,
    50,
    51,
    52,
    53,
    54,
    55,
    56,
    57,
    58,
    59,
    60,
    61,
    62,
    63,
    64,
    65,
    66,
    67,
    68,
    69,
    70,
    71,
    72,
    73,
    74,
    75,
    76,
    77,
    78,
    79,
    80,
    81,
    82,
    83,
    84,
    85,
    86,
    87,
    88,
    89,
    90,
    91,
    92,
    93,
    94,
    95,
    96,
    97,
    98,
    99,
    100,
]
for i in inputs:
    result = Solution().getMaximumGenerated(i)
    correct_result = Solution().leetcode_solution(i)
    if result == correct_result:
        print('✔️    ', result, '    👉  ', correct_result)
    else:
        print('❌   ', result, '    👉  ', correct_result)
