# Redo

class Solution:
    def subsetsWithDup(self, nums: [int]) -> [[int]]:
        nums = sorted(nums)
        result = []

        def rec(depth: int, currArr):
            if depth >= len(nums):
                result.append(tuple(currArr))
                return
            for i in range(depth, len(nums)):
                rec(i + 1, currArr[:] + [nums[i]])
            result.append(tuple(currArr))
        rec(0, [])
        return set(result)

    # Correct solution from leetcode to check my answers against my test-cases.
    def leetcode_correct_solution():
        pass


inputs = [
    # [1, 2, 2],
    # [1, 2],
    [4, 4, 4, 1, 4]
]

for i in inputs:
    result = Solution().subsetsWithDup(i)
    correct_result = 0  # Solution().leetcode_correct_solution(i)
    if result == correct_result:
        print('✔️    ', result, '    👉  ', correct_result)
    else:
        print('❌   ', result, '    👉  ', correct_result)

# EXAMPLE TEST CASES
#
