class Solution:
    def combinationSum_v2(self, candidates: [int], target: int) -> [[int]]:
        result = []

        def rec(arr: [int], currCandidateIndex):
            if sum(arr) > target:
                return
            if sum(arr) == target:
                result.append(arr)
                return
            rec(arr + [candidates[currCandidateIndex]], currCandidateIndex)
            if currCandidateIndex + 1 < len(candidates):
                rec(arr, currCandidateIndex + 1)
        rec([], 0)
        return result

    # works but it's extreamly slow.
    def combinationSum(self, candidates: [int], target: int) -> [[int]]:
        def rec(t: [int]):
            if t == 0:
                return []
            result = []
            for i in range(1, t + 1):
                options = rec(t - i)
                if not len(options) == 0:
                    for opt in options:
                        result.append([i] + opt)
                else:
                    result.append([i])
            return result

        allPosibilities = rec(target)
        temp = [tuple(sorted(i)) for i in allPosibilities]
        temp = set(temp)
        result = [i for i in temp if set(i).issubset(candidates)]
        return result

    # Correct solution from leetcode to check my answers against my test-cases.
    def leetcode_correct_solution():
        pass


inputs = [
    [[1, 2, 7], 7],
    [[2, 3, 6, 7], 7],
    # [[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30], 10],
    # [[5, 10, 8, 4, 3, 12, 9], 27],

]

for i, target in inputs:
    # continue
    result = Solution().combinationSum_v2(i, target)
    correct_result = 0  # Solution().leetcode_correct_solution(i)
    if result == correct_result:
        print('✔️    ', result, '    👉  ', correct_result)
    else:
        print('❌   ', result, '    👉  ', correct_result)
