#

class Solution:
    def combinationSum2(self, candidates: [int], target: int) -> [[int]]:
        candidates = sorted(candidates)
        result = []

        def recTree(candiIndex, subarr):
            arrSum = sum(subarr)
            if arrSum > target:
                return
            if candiIndex >= len(candidates) or arrSum == target:
                if subarr not in result and arrSum == target:
                    result.append(subarr[::])
                return

            predonePaths = []
            for i in range(candiIndex, len(candidates)):
                if candidates[i] in predonePaths:
                    continue
                predonePaths.append(candidates[candiIndex])
                recTree(i + 1, subarr + [candidates[i]])
        recTree(0, [])
        return result

    # This one was by chatGPT and as far as I know it works just fine while also having high efficiency. Pretty cool, I did not think that it would even work.
    def combinationSum2_byChatGPT(self, candidates: [int], target: int) -> [[int]]:

        candidates = sorted(candidates)
        result = []
        memo = {}

        def recTree(candiIndex, subarr, arrSum):
            if (candiIndex, arrSum) in memo:
                return memo[(candiIndex, arrSum)]
            if candiIndex >= len(candidates) or arrSum == target:
                if subarr not in result and arrSum == target:
                    result.append(subarr[::])
                return
            predonePaths = []
            for i in range(candiIndex, len(candidates)):
                if candidates[i] in predonePaths:
                    continue
                predonePaths.append(candidates[candiIndex])
                recTree(i + 1, subarr + [candidates[i]], arrSum + candidates[i])
            memo[(candiIndex, arrSum)] = result
            return result

        recTree(0, [], 0)
        return result

    # Correct solution from leetcode to check my answers against my test-cases.
    def leetcode_correct_solution():
        # Just use the chatGPT solution instead of this.
        pass


inputs = [
    [[10, 1, 2, 7, 6, 1, 5], 8],
    [[1, 1, 2, 5, 6, 7, 10], 8],
    [[1, 2, 1, 2], 2],
    [[1], 2],
    [[1, 2, 3], 3],
    [[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], 27],
    [[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], 5],
    [[14, 6, 25, 9, 30, 20, 33, 34, 28, 30, 16, 12, 31, 9, 9, 12, 34, 16, 25, 32, 8, 7, 30, 12, 33, 20, 21, 29, 24,
      17, 27, 34, 11, 17, 30, 6, 32, 21, 27, 17, 16, 8, 24, 12, 12, 28, 11, 33, 10, 32, 22, 13, 34, 18, 12], 27],
    [[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 1,
        2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50], 30]
]

for i, target in inputs:
    result = Solution().combinationSum2(i, target)
    correct_result = Solution().combinationSum2_byChatGPT(i, target)  # Solution().leetcode_correct_solution(i)
    if result == correct_result:
        print('✔️    ', result, '    👉  ', correct_result)
    else:
        print('❌   ', result, '    👉  ', correct_result)

# EXAMPLE TEST CASES
#
