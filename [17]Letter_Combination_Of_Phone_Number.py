#

class Solution:
    numToStrMap = {
        2: "abc",
        3: "def",
        4: "ghi",
        5: "jkl",
        6: "mno",
        7: "pqrs",
        8: "tuv",
        9: "wxyz"
    }

    def letterCombinations(self, digits: str) -> [str]:
        if not len(digits):
            return []
        digitsToLetters = [Solution.numToStrMap[int(digit)] for digit in digits]
        result = []

        def rec(letterGroup: int, combo: str):
            if letterGroup >= len(digitsToLetters):
                return result.append(combo)
            for i in range(len(digitsToLetters[letterGroup])):
                rec(letterGroup + 1, combo + digitsToLetters[letterGroup][i])
        rec(0, '')
        return result

    # Correct solution from leetcode to check my answers against my test-cases.
    def leetcode_correct_solution(self, digits: str):
        d = {"0": "0", "2": "abc", "3": "def", "4": "ghi",
             "5": "jkl", "6": "mno", "7": "pqrs",
             "8": "tuv", "9": "wxyz"}
        n = len(digits)
        if n == 0:
            return []
        elif n == 3:
            digits += "0"
        elif n == 2:
            digits += "00"
        elif n == 1:
            digits += "000"
        ans = []
        for a in d[digits[0]]:
            if a != "0":
                for b in d[digits[1]]:
                    if b != "0":
                        for c in d[digits[2]]:
                            if c != "0":
                                for x in d[digits[3]]:
                                    if x != "0":
                                        ans.append(a + b + c + x)
                                    else:
                                        ans.append(a + b + c)
                            else:
                                ans.append(a + b)
                    else:
                        ans.append(a)
        return ans
        pass


inputs = [
    "234",
    "2",
    "9234",
    "2934",
    "3924",
    "9324",
    "2394",
    "3294",
    "3249",
    "2349",
    "4329",
    "3429",
    "2439",
    "4239",
    "4932",
    "9432",
    "3492",
    "4392",
    "9342",
    "3942",
    "2943",
    "9243",
    "4293",
    "2493",
    "9423",
    "4923",
    "9876"
]

for i in inputs:
    result = Solution().letterCombinations(i)
    correct_result = Solution().leetcode_correct_solution(i)
    if result == correct_result:
        print('✔️    ', result, '    👉  ', correct_result)
    else:
        print('❌   ', result, '    👉  ', correct_result)

# EXAMPLE TEST CASES
#

