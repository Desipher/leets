from collections import deque
import collections
from typing import List


class Solution:
    def dailyTemperatures(self, temperatures: List[int]):
        stk = deque()
        result = deque()
        for index in range(len(temperatures)-1, -1, -1):
            while stk:
                i, val = stk[-1] 
                if val > temperatures[index]:
                    result.appendleft(i - index)
                    break
                else:
                    stk.pop()
            else:
                result.appendleft(0)
            
            stk.append([index, temperatures[index]])
        return result

        

    def dailyTemperatures_BruteForce(self, temperatures: List[int]):
        answers = []
        for index, temp in enumerate(temperatures):
            i = 0
            for j in range(index+1, len(temperatures)):
                i+=1
                if temperatures[j] > temp:
                    answers.append(i)
                    break
            else:
                answers.append(0)
        return answers




temperatures= [
    [73,74,75,71,69,72,76,73],
]

for temp in temperatures:
    print(Solution().dailyTemperatures(temp))

# li = collections.deque([[11,22], [88, 99]])
# x,y = li.pop()
# print(x, y)