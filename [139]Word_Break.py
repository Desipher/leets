# Redo

class Solution:
    def wordBreak_bottomUp(self, s: str, wordDict: [str]) -> bool:
        dp = [False] * (len(s) + 1)
        dp[-1] = True

        for i in range(len(s) - 1, -1, -1):
            for word in wordDict:
                if s[i:i + len(word)] == word:
                    dp[i] = dp[i + len(word)]
                if dp[i]:
                    break

        return dp[0]

    def wordBreak(self, s: str, wordDict: [str]) -> bool:
        def rec(index):
            if index > len(s):
                return False
            if index == len(s):
                return True
            result = False
            for word in wordDict:
                if s[index:index + len(word)] == word:
                    temp = rec(index + len(word))
                    result = temp if temp else result
            return result

        return rec(0)

    def wordBreak_suboptimal(self, s: str, wordDict: [str]) -> bool:
        allCombos = []

        def rec(s):
            if 1:
                return
            for x in range(1, len(s)):
                allCombos.append([s[:x], s[x:]])
                rec(s[:])
        rec(s)
        return True

    # Correct solution from leetcode to check my answers against my test-cases.
    def leetcode_correct_solution(self, s: str, wordDict: [str]):
        wordSet = set(wordDict)
        n = len(s)

        dp = [False] * (n + 1)
        dp[n] = True
        for i in range(n - 1, -1, -1):
            for j in range(i + 1, n + 1):
                if dp[j] and s[i:j] in wordSet:
                    dp[i] = True
                    break

        return dp[0]


inputs = [
    ["leetcode", ["leet", "code"]],
    ["applepenapple", ["apple", "pen"]],
    ["catsandog", ["and", "cats", "dog", "sand", "cat"]],
    ["cars", ["car", "ca", "rs"]],
    ["abcd", ["a", "abc", "b", "cd"]],
    ["a", ["b"]],
    ["aa", ["a", "b", "aa"]],
    ["aa", ["a", "b"]],
    ["afdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdx",
        "afdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdxafdx"]
]

for i, wordDict in inputs:
    result = Solution().wordBreak_bottomUp(i, wordDict)
    correct_result = Solution().leetcode_correct_solution(i, wordDict)
    if result == correct_result:
        print('✔️    ', result, '    👉  ', correct_result)
    else:
        print('❌   ', result, '    👉  ', correct_result)

# EXAMPLE TEST CASES
#
