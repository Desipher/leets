from typing import List


class Solution:

    def rob_v3(self, nums: List[int]) -> int:  # My latest greatest solution
        prev = 0
        curr = nums[0]
        for n in nums[1:]:
            new_value = max(curr, n + prev)
            prev = curr
            curr = new_value
        return curr

    def rob_v2_iterative(self, nums: List[int]) -> int:
        if len(nums) <= 2:
            return max(nums)
        robbed = [0] * (len(nums) + 1)
        robbed[1] = nums[0]
        r = 2
        for n in nums[1:]:
            robbed[r] = max(robbed[r - 1], robbed[r - 2] + n)
            r += 1
        return robbed[-1]

    def rob_v2(self, nums: List[int]) -> int:  # I Just came back after a long time and tried redoing this problem. Hence the v2.0
        if len(nums) <= 2:
            return max(nums)
        memo = {}

        def rob(ptr, money_robbed):
            if ptr >= len(nums):
                return 0
            if ptr in memo.keys():
                return memo[ptr]
            memo[ptr] = max(rob(ptr + 2, money_robbed), rob(ptr + 3, money_robbed)) + money_robbed + nums[ptr]
            return memo[ptr]
        return max(rob(0, 0), rob(1, 0))

    def rob(self, nums: List[int]) -> int:
        left, mid, right = 0, 1, 2
        result = [0]
        while right < len(nums):
            if nums[left] < nums[right] and nums[mid] < nums[right]:
                res = nums[right] + result[-1]
                left = right + 1
                mid = right + 2
                right += 3
            elif nums[left] < nums[mid] and nums[right] < nums[mid]:
                res = mid + result[-1]
                left = mid + 1
                mid = mid + 2
                right = mid + 3
            else:
                res = left + result[-1]
                left = left + 1
                mid = left + 2
                right = left + 3
            result.append(res)
        return result[-1]

    def rob_recursive(self, nums: List[int]) -> int:
        if len(nums) == 1:
            return nums[0]
        dict = {}

        def getRobbed(ptr):
            if ptr >= len(nums):
                return 0
            if ptr in dict.keys():
                return dict[ptr]
            dict[ptr] = nums[ptr] + max(getRobbed(ptr + 2), getRobbed(ptr + 3))
            return dict[ptr]

        return getRobbed(-2) - nums[-2]


money = [
    ([1, 2, 3, 1], 4),
    ([2, 7, 9, 3, 1], 12),
    ([0], 0),
    ([2, 7, 9, 3, 1, 3, 4, 21], 35),
    ([1, 2], 2),
    ([1], 1),
    ([1, 3, 1], 3),
    ([1, 4, 1], 4),
    ([1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5,
     6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9], 265)
]
for m, ans in money:
    print(Solution().rob_v3(m), ans)
