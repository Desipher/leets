from ctypes import pointer
from multiprocessing.sharedctypes import Value
from turtle import position, right
from typing import List


class Solution:
    def carFleet(self, target: int, position: List[int], speed: List[int]) -> int:
        posSpeedHm = {key:value for key, value in zip(position, speed)}
        position.sort()

        fleet = 1
        right = len(position)-1
        for left in range(len(position)-2, -1, -1):
            behind_finishTime = (target - position[left])/posSpeedHm[position[left]]
            front_finishTime = (target - position[right])/posSpeedHm[position[right]]
            if behind_finishTime > front_finishTime:
                fleet += 1
                right = left
        return fleet


inputs = [
    [12, [0, 3, 5, 8, 10], [1, 3, 1, 4, 2]],
    # [100, [0, 2, 4], [4, 2, 1]],
    # [10, [6, 8], [3, 2]],
    # [100, [0, 2, 4], [4, 2, 1]],
    # [10,[2, 4],[3, 2]]
]

for target, position, speed in inputs:
    print(Solution().carFleet(target, position, speed))
